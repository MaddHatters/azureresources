﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;

namespace AzureFunctionsApp.Managers
{
    public interface IQueueManager {
        void QueueMessage(string message);
        CloudStorageAccount GetCloudStorageAccount();
    }

    public class QueueManager : IQueueManager
    {
        private ExecutionContext _executionContext;
        private ILogger _log;

        public QueueManager(ExecutionContext executionContext, ILogger log) {
            _log = log;
            _executionContext = executionContext;
            CreateQueueIfNotExists();
        }

        public async void QueueMessage(string message)
        {

            // TODO: [MB] Investigate the pros and cons of building the message here. We probably want to offload this elsewhere.
                //string randomStr = Guid.NewGuid().ToString();
                //var serializeJsonObject = JsonConvert.SerializeObject(
                //    new
                //    {
                //        ID = randomStr,
                //        Content = $"<html><body><h2> {message} </h2></body></html>"
                //    });

            CloudStorageAccount storageAccount = GetCloudStorageAccount();
            CloudQueueClient cloudQueueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue cloudQueue = cloudQueueClient.GetQueueReference("email-queue");
            var cloudQueueMessage = new CloudQueueMessage(message);
            await cloudQueue.AddMessageAsync(cloudQueueMessage);
        }

        public CloudStorageAccount GetCloudStorageAccount()
        {
            string connectionString = Environment.GetEnvironmentVariable("AzureStorageConnectionString");
            _log.LogInformation($"ConnectionString: {connectionString}");
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            return storageAccount;
        }

        private void CreateQueueIfNotExists()
        {
            CloudStorageAccount storageAccount = GetCloudStorageAccount();
            CloudQueueClient cloudQueueClient = storageAccount.CreateCloudQueueClient();
            string[] queues = new string[] { "email-queue" };
            foreach (var item in queues)
            {
                CloudQueue cloudQueue = cloudQueueClient.GetQueueReference(item);
                cloudQueue.CreateIfNotExistsAsync();
            }
        }
    }
}
