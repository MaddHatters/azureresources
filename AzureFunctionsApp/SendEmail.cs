// The 'From' and 'To' fields are automatically populated with the values specified by the binding settings.
//
// You can also optionally configure the default From/To addresses globally via host.config, e.g.:
//
// {
//   "sendGrid": {
//      "to": "user@host.com",
//      "from": "Azure Functions <samples@functions.com>"
//   }
// }
using Microsoft.Azure.WebJobs;
using SendGrid.Helpers.Mail;
using Microsoft.Extensions.Logging;

namespace AzureFunctionsApp
{
    public static class SendEmail
    {
        [FunctionName("SendEmail")]
        [return: SendGrid(ApiKey = "SendGridApiKey")]
        public static SendGridMessage Run([QueueTrigger("email-queue", Connection = "AzureStorageConnectionString")] User user, ILogger log)
        {
            log.LogInformation($"C# Queue trigger function processed user: {user.FirstName} {user.LastName}");
            SendGridMessage message = new SendGridMessage()
            {
                Subject = $"You Registered",
                From = new EmailAddress("mabeauli@gmail.com", "AzurePracticeFunctionApp")
            };

            // TODO: [Matt B] Add a custom email template.
                // A central template generator would also allow us to keep the templates consistent
                // Maybe we could use SendGrid templates
                // The logic to create the template should/could be abstracted so this function can focus on sending emails and not formatting them.
            message.AddTo(user.Email);
            message.AddContent("text/plain", $"{user.FirstName} {user.LastName}, you registered successfully.");
            return message;
        }
    }
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
