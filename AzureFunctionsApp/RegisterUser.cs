using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using AzureFunctionsApp.Managers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Abstractions;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Resolvers;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AzureFunctionsApp
{

    public static class RegisterUser
    {
        [FunctionName("RegisterUser")]
        [OpenApiOperation(operationId: "RegisterUser", tags: new[] { "Registration" })]
        [OpenApiRequestBody("application/json", typeof(RegisterUserDto), Required = true, Description = "Body Descrip")]
        [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: "text/plain", bodyType: typeof(string), Description = "The OK response")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            string responseMessage;
            try {
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                RegisterUserDto data = JsonConvert.DeserializeObject<RegisterUserDto>(requestBody);
                string firstName = data?.FirstName;
                string lastName = data?.LastName;
                string email = data?.Email;

                if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(lastName) || string.IsNullOrWhiteSpace(email)) {
                    return new BadRequestObjectResult($"Please enter your name and email and try again.");
                }

                var manager = new QueueManager(context, log);
                manager.QueueMessage(requestBody);

                responseMessage = $"Hi {firstName} {lastName}! You registered successfully. You should receive a confirmation email at {email} shortly.";
            }
            catch (Exception e) {
                log.LogError($"Failed to register user: {e.Message}", e);
                var ex = new Exception($"An error occurred. Please ensure your name and email were entered correctly and try again.");
                return new ExceptionResult(ex, true);
            }

            return new OkObjectResult(responseMessage);
        }

        [OpenApiExample(typeof(RegisterUserDtoExample))]
        public class RegisterUserDto
        {
            /// <summary>The id of the customer in the context. This is also called payer, sub_account_id.</summary>
            [Newtonsoft.Json.JsonProperty("FirstName", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string FirstName { get; set; }

            [Newtonsoft.Json.JsonProperty("LastName", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string LastName { get; set; }

            /// <summary>The order number. Used to uniquely identify a group of order lines.</summary>
            [Newtonsoft.Json.JsonProperty("Email", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string Email { get; set; }
        }

        public class RegisterUserDtoExample : OpenApiExample<RegisterUserDto>
        {
            public override IOpenApiExample<RegisterUserDto> Build(NamingStrategy namingStrategy = null)
            {
                this.Examples.Add(
                    OpenApiExampleResolver.Resolve(
                        "RegisterUserDtoExample",
                        new RegisterUserDto()
                        {
                            FirstName = "John",
                            LastName = "Doe",
                            Email = "johnDoe@gmail.com"
                        },
                        namingStrategy
                    ));

                return this;
            }
        }
    }
}

