﻿using AzureWebApp.Models;
using AzureWebApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace AzureWebApp.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class ApplicationUserController : ControllerBase
    {
        private readonly IApplicationUserService _applicationUserService;
        private readonly IQueueService _queueService;
        private readonly ILogger _logger;

        public ApplicationUserController(IApplicationUserService applicationUserService, IQueueService queueService) {
            _applicationUserService = applicationUserService;
            _queueService = queueService;

            // TODO: Add ability to inject logger
        }

        [HttpGet]
        [Route("allUsers")]
        public IActionResult GetUsers()
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        [Route("getUser/{email}")]
        public async Task<IActionResult> GetUserByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return BadRequest();

            ApplicationUser user;
            // TODO: Add a global error handler
            try
            {
                user = await _applicationUserService.GetUserByEmail(email);
            }
            catch (Exception e)
            {
                //_logger.LogError("Error occurred while registering user", e);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok(user);
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterUser(ApplicationUser user) {
            if (string.IsNullOrWhiteSpace(user.FirstName) || string.IsNullOrWhiteSpace(user.LastName) || string.IsNullOrWhiteSpace(user.Email))
                return BadRequest();

            try {
                // Save user to DB
                await _applicationUserService.RegisterUser(user);

                // Add email to queue
                _queueService.QueueMessage(JsonConvert.SerializeObject(user));

            } catch (Exception e) {
                //_logger.LogError("Error occurred while registering user", e);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }
    }
}
