﻿using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;

namespace AzureWebApp.Services
{
    public interface IQueueService
    {
        void QueueMessage(string message);
        CloudStorageAccount GetCloudStorageAccount();
    }

    public class QueueService : IQueueService
    {
        private IConfiguration _configuration;
        private readonly string _queueName = "email-queue";

        public QueueService(IConfiguration configuration)
        {
            _configuration = configuration;
            CreateQueueIfNotExists();
        }

        public async void QueueMessage(string message)
        {
            CloudStorageAccount storageAccount = GetCloudStorageAccount();
            CloudQueueClient cloudQueueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue cloudQueue = cloudQueueClient.GetQueueReference(_queueName);
            var cloudQueueMessage = new CloudQueueMessage(message);
            await cloudQueue.AddMessageAsync(cloudQueueMessage);
        }

        public CloudStorageAccount GetCloudStorageAccount()
        {
            var connString = _configuration.GetConnectionString("AzureStorageConnectionString");
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connString);
            return storageAccount;
        }

        private void CreateQueueIfNotExists()
        {
            CloudStorageAccount storageAccount = GetCloudStorageAccount();
            CloudQueueClient cloudQueueClient = storageAccount.CreateCloudQueueClient();
            string[] queues = new string[] { _queueName };
            foreach (var item in queues)
            {
                CloudQueue cloudQueue = cloudQueueClient.GetQueueReference(item);
                cloudQueue.CreateIfNotExistsAsync();
            }
        }
    }

}
