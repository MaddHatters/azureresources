﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureWebApp.Models;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Fluent;
using Microsoft.Extensions.Configuration;

namespace AzureWebApp.Services
{
    public interface ICosmosDbService
    {
        Task<IEnumerable<ApplicationUser>> GetApplicationUsersAsync(string query);
        Task<ApplicationUser> GetApplicationUserByEmailAsync(string email);
        Task AddApplicationUserAsync(ApplicationUser item);
        Task UpdateApplicationUserAsync(string id, ApplicationUser item);
        Task DeleteApplicationUserAsync(string id);
    }

    public class CosmosDbService : ICosmosDbService
    {
        private Container _container;

        public CosmosDbService(IConfiguration configuration)
        {
            if (_container is null) {
                _container = InitializeCosmosClientInstanceAsync(configuration.GetSection("CosmosDbSqlCore")).GetAwaiter().GetResult();
            }
        }

        public async Task AddApplicationUserAsync(ApplicationUser item)
        {
            item.Id = item.Email;
            await this._container.CreateItemAsync<ApplicationUser>(item, new PartitionKey(item.Email));
        }

        public async Task DeleteApplicationUserAsync(string email)
        {
            await this._container.DeleteItemAsync<ApplicationUser>(email, new PartitionKey(email));
        }

        public async Task<ApplicationUser> GetApplicationUserByEmailAsync(string email)
        {
            try
            {
                ItemResponse<ApplicationUser> response = await this._container.ReadItemAsync<ApplicationUser>(email, new PartitionKey(email));
                return response.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }
        }

        public async Task<IEnumerable<ApplicationUser>> GetApplicationUsersAsync(string queryString)
        {
            var query = this._container.GetItemQueryIterator<ApplicationUser>(new QueryDefinition(queryString));
            List<ApplicationUser> results = new List<ApplicationUser>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();

                results.AddRange(response.ToList());
            }

            return results;
        }

        public async Task UpdateApplicationUserAsync(string id, ApplicationUser user)
        {
            await this._container.UpsertItemAsync<ApplicationUser>(user, new PartitionKey(user.Email));
        }

        /// <summary>
        /// Creates a Cosmos DB database and a container with the specified partition key. 
        /// </summary>
        /// <returns></returns>
        private static async Task<Container> InitializeCosmosClientInstanceAsync(IConfigurationSection configurationSection)
        {
            string databaseName = configurationSection.GetSection("DatabaseName").Value;
            string containerName = configurationSection.GetSection("ContainerName").Value;
            string account = configurationSection.GetSection("Account").Value;
            string key = configurationSection.GetSection("Key").Value;
            CosmosClient client = new CosmosClient(account, key);
            DatabaseResponse database = await client.CreateDatabaseIfNotExistsAsync(databaseName);
            await database.Database.CreateContainerIfNotExistsAsync(containerName, "/Email");
            var container = client.GetContainer(databaseName, containerName);
            return container;
        }
    }
}
