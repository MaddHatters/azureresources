﻿using AzureWebApp.Models;
using System.Threading.Tasks;

namespace AzureWebApp.Services
{
    public interface IApplicationUserService {
        Task RegisterUser(ApplicationUser user);
        Task<ApplicationUser> GetUserByEmail(string email);
    }
    public class ApplicationUserService : IApplicationUserService
    {
        private readonly ICosmosDbService _cosmosDbService;
        public ApplicationUserService(ICosmosDbService cosmosDbService) {
            _cosmosDbService = cosmosDbService;
        }

        public async Task RegisterUser(ApplicationUser user) {
            await _cosmosDbService.AddApplicationUserAsync(user);
        }

        public async Task<ApplicationUser> GetUserByEmail(string email)
        {
            return await _cosmosDbService.GetApplicationUserByEmailAsync(email);
        }
    }
}
