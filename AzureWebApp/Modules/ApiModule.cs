﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using AzureWebApp.Services;

namespace AzureWebApp.Modules
{
    public class ApiModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assemblies = new List<Assembly> { Assembly.GetExecutingAssembly() };
            builder.RegisterAssemblyTypes(assemblies.OrderBy(s => s.FullName).ToArray()).AsSelf().AsImplementedInterfaces()
                .Except<CosmosDbService>(db => db.As<ICosmosDbService>().SingleInstance());
        }
    }
}
