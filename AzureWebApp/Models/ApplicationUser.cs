﻿using Newtonsoft.Json;

namespace AzureWebApp.Models
{
    public class ApplicationUser
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; } = false;
    }
}
