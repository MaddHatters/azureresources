import MiniDrawer from './components/MiniDrawer/MiniDrawer';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import './App.css';
import LoginPage from './pages/LoginPage/LoginPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <Switch>
            <Route exact path="/">
              <Redirect to="/login" />
            </Route>
            <Route exact path="/login">
              <LoginPage/>
            </Route>
            <Route exact path="/register">
              <RegisterPage/>
            </Route>
            <Route exact path="/registration">
              <RegisterPage/>
            </Route>
            <Route path="/dashboard">
              <MiniDrawer/>
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;