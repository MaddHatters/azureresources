import styles from './DashboardPage.module.css';

const DashboardPage = (props: any) => (
  <div className={styles.DashboardPage} data-testid='DashboardPage'>
    {props.name}
  </div>
);

export default DashboardPage;
