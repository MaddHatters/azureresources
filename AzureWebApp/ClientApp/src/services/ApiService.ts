import AppSettings from '../appSettings.json';
import UserModel from '../models/UserModel';
import axios, {AxiosResponse} from 'axios';

class ApiService {

    async GetUserAsync(email: string): Promise<UserModel> {
        let promise = await axios.get<UserModel>(`${AppSettings.WebAppBaseUrl}/api/user/getUser/${email}`)
            .then((response: AxiosResponse) => {
                return response.data;
            }).catch(() => { return null});
        return promise;
    }

    async GetUsersAsync(): Promise<UserModel[]> {
        let promise = await axios.get<UserModel[]>(`${AppSettings.WebAppBaseUrl}/api/user/allUsers`)
            .then((response: AxiosResponse) => {
                return response.data;
            }).catch(() => { return null});
        return promise;
    }

    async RegisterUserAsync(user: UserModel): Promise<boolean> {
        let promise = await axios.post(`${AppSettings.WebAppBaseUrl}/api/user/register`, user)
            .then(() => {
                return true;
            })
            .catch(() => {
                return false;
            });

        return promise;
    }
}


export default ApiService;