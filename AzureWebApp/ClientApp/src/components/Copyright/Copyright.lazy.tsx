import React, { lazy, Suspense } from 'react';

const LazyCopyright = lazy(() => import('./Copyright'));

const Copyright = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyCopyright {...props} />
  </Suspense>
);

export default Copyright;
