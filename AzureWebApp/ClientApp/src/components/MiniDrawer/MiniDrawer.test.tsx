import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import MiniDrawer from './MiniDrawer';

describe('<MiniDrawer />', () => {
  test('it should mount', () => {
    render(<MiniDrawer />);
    
    const miniDrawer = screen.getByTestId('MiniDrawer');

    expect(miniDrawer).toBeInTheDocument();
  });
});