import React, { lazy, Suspense } from 'react';

const LazyMiniDrawer = lazy(() => import('./MiniDrawer'));

const MiniDrawer = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyMiniDrawer {...props} />
  </Suspense>
);

export default MiniDrawer;
