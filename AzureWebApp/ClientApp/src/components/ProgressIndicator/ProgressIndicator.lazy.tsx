import React, { lazy, Suspense } from 'react';

const LazyProgressIndicator = lazy(() => import('./ProgressIndicator'));

const ProgressIndicator = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyProgressIndicator {...props} />
  </Suspense>
);

export default ProgressIndicator;
