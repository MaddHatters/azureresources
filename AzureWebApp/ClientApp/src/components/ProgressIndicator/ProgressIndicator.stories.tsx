/* eslint-disable */
import ProgressIndicator from './ProgressIndicator';

export default {
  title: "ProgressIndicator",
};

export const Default = () => <ProgressIndicator />;

Default.story = {
  name: 'default',
};
