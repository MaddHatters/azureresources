import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Fade from '@mui/material/Fade';

const ProgressIndicator = function(props: any) {
  return(
    <Backdrop sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={props.loading}>

    <Fade
        in={props.loading}
        style={{
          transitionDelay: props.loading ? '200ms' : '0ms',
        }}
        unmountOnExit
      >
        <CircularProgress color="inherit" />
    </Fade>
  </Backdrop>);
};

export default ProgressIndicator;
