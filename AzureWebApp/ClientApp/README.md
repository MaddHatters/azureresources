# Azure Practie App Documentation
    - [Default React Readme](./documentation/DefaultReactDocumentation.md)

## Generate Components
    - https://www.npmjs.com/package/generate-react-cli
    - npx generate-react-cli component Box
        - Component: npx generate-react-cli component <name>
        - Page: npx generate-react-cli component <name> --type=page
        - Layout: npx generate-react-cli component <name> --type=layout

## UI
    - https://www.material-ui.com/

## Helpful Docs
    - https://react-typescript-cheatsheet.netlify.app/docs/advanced/patterns_by_version