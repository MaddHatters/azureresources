# AzurePractice
	- Learning Azure and React.
 
## Components
	- AzureFunctionsApp
		- Azure HTTP Function which adds a user object to an email queue
			- OpenAPI supported out of the box.
			- https://azurepracticefunctionapp.azurewebsites.net/api/swagger/ui
		- Azure SendGrid Queue Trigger
			- Consumes data in the email queue and sends an email using SendGrid
			
	- AzureWebApp
		- https://azurepracticewebapp.azurewebsites.net/
		- .Net Core 3.1 with autofac for DI
		- Baseline React app with slight modifications
			- Material-ui for frontend UI library
			- Typescript
		- Allows users to register and receive a confirmation email via the email queue referenced above and teh Azure SendGrid Queue Trigger
		- Allows users to log in and view the dashboard
		
	- Azure Cosmos DB
		- Fast scalable NoSql data store for persisting the user info after registration
		
## Frontend Readme
	- [Frontend Readme](./AzureWebApp/ClientApp/README.md)
